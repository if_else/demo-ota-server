package com.spd.demootaserver.web

import org.apache.commons.compress.utils.IOUtils
import org.slf4j.LoggerFactory
import org.springframework.core.io.ClassPathResource
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.io.IOException
import javax.servlet.http.HttpServletResponse
import kotlin.jvm.Throws


@RestController
@RequestMapping
class OtaController {

    private val logger = LoggerFactory.getLogger(javaClass)
    private val NEW_FIRMWARE_NAME = "arduino-files/file.v.0.0.2.bin"

    @GetMapping(value = ["/firmware"])
    @Throws(IOException::class)
    fun getFirmware(@RequestHeader headers: Map<String, String>,
                    response: HttpServletResponse): ByteArray? {
        logger.trace("Checking firmware version... headers={}", headers)
        val currentVersion = headers["x-esp8266-version"]
        val currentVersionDigits = getVersionDigits(currentVersion!!)
        val newVersionDigits = getVersionDigits(NEW_FIRMWARE_NAME)

        if (currentVersionDigits >= newVersionDigits) {
            logger.trace("The firmware version is up-to-date headers={}", headers)
            response.status = 304
            return null
        }

        val resource = ClassPathResource(NEW_FIRMWARE_NAME)
        val inputStream = resource.inputStream
        logger.trace("New firmware found, sending... headers={} newFirmwareVersion={}", headers, NEW_FIRMWARE_NAME)
        return IOUtils.toByteArray(inputStream)
    }

    private fun getVersionDigits(version: String): Int {
        val versionNumbers = version.replace("[^0-9]".toRegex(), "")
        return Integer.valueOf(versionNumbers)
    }

}
