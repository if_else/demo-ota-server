package com.spd.demootaserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoOtaServerApplication

fun main(args: Array<String>) {
	runApplication<DemoOtaServerApplication>(*args)
}
