Kotlin. Java 11. Spring.

Project created for demostration HTTP OTA update esp8266 microcontroller.

What server can do?
The server can respond with a byte array to the request. Server will send file only if current version less then new and 304 status if not(according to esp8266 doc).

What can do firmwares?
Firmwares are supports Ota http updates and they should blinking with different frequency(0.0.1 - 1000ms, 0.0.1 - 200ms)

Short Description:
1. Here are two bin files with HTTP OTA firmware and httpUpdate.ino file in resources;
2. Firmware v.0.0.1 delay - 1000ms and v.0.0.2 - 200ms;

How to start:
1. Download and install Arduino app (maybe you'll need python)
2. Open file httpUpdate.ino in Arduino app
3. Set up board esp8266(you can find easy tutorials in google)
5. include libriary https://github.com/nrwiersma/ESP8266Scheduler (You can do this threw Library Manager)
5. change this APSSID, APPSK, "http://192.168.0.2:8080/firmware" to yours
6. Connect esp8266 using usb to pc and press Upload;
7. Then (or before all steps) you can start server.

Esp8266 will make requests every 30 seconds, on version 0.0.1 light will blinking every 1 second, after update(0.0.2) every 0.2 second.
You may add another version, but this version must be higher that the one installed on device.

Request example:
curl --location --request GET 'http://localhost:8080/firmware' \
--header 'x-esp8266-version: v.0.0.2'
